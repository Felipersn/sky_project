package felipesilveira.skyproject.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import felipesilveira.skyproject.databinding.ListitemMovieCardBinding
import felipesilveira.skyproject.model.MoviesList

class MovieListAdapter(var movies: List<MoviesList>) : RecyclerView.Adapter<MovieListAdapter.ViewHolder>(), AdapterItemContract {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ListitemMovieCardBinding = ListitemMovieCardBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun replaceItems(items: List<*>) {
        this.movies = items as List<MoviesList>
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    class ViewHolder(val binding: ListitemMovieCardBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: MoviesList) {
            binding.movie = movie
            binding.executePendingBindings()
        }
    }
}