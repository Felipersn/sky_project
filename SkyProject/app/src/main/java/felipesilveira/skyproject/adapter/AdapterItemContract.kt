package felipesilveira.skyproject.adapter

interface AdapterItemContract {
    fun replaceItems(list: List<*>)
}