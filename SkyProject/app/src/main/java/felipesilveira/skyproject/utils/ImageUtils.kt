package felipesilveira.skyproject.utils

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import felipesilveira.skyproject.R

@BindingAdapter("bind:cover_url")
fun loadImage(view: ImageView, url: String) {
    Glide.with(view.context)
            .asBitmap()
            .load(url)
            .transition(withCrossFade())
            .apply(RequestOptions().placeholder(R.drawable.placeholder))
            .into(view)
}

