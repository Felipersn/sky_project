package felipesilveira.skyproject.connection

import felipesilveira.skyproject.model.MoviesList
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {
    @GET("api/Movies")
    fun getMoviesList(): Call<List<MoviesList>>
}