package felipesilveira.skyproject.connection

import android.util.Log
import felipesilveira.skyproject.model.MoviesList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataSource(val apiInterface: ApiInterface): MoviesListDataSource {
    override fun listAll(success: (List<MoviesList>) -> Unit, failure: () -> Unit) {
        val call = apiInterface.getMoviesList()
        call.enqueue(object : Callback<List<MoviesList>> {
            override fun onResponse(call: Call<List<MoviesList>>, response: Response<List<MoviesList>>) {
                if (response.isSuccessful) {

                    Log.i("RESPONSE", response.body().toString())

                    val movies = mutableListOf<MoviesList>()
                    response.body()!!.forEach {
                        movies.add(it)
                    }
                    success(movies)
                } else {
                    failure()
                }
            }

            override fun onFailure(call: Call<List<MoviesList>>, t: Throwable?) {
                Log.i("RESPONSE", t.toString())
                failure()
            }
        })

    }

}