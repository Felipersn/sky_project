package felipesilveira.skyproject.connection

import felipesilveira.skyproject.model.MoviesList

class MoviesRepository(private val dataSource: MoviesListDataSource): MoviesListDataSource {

    override fun listAll(success: (List<MoviesList>) -> Unit, failure: () -> Unit) {
        dataSource.listAll(success, failure)
    }
}