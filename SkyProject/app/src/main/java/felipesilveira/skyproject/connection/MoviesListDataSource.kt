package felipesilveira.skyproject.connection

import felipesilveira.skyproject.model.MoviesList

interface MoviesListDataSource {
    fun listAll(success : (List<MoviesList>) -> Unit, failure: () -> Unit)
}