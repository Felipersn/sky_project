package felipesilveira.skyproject.model

import java.util.*

class MoviesList: Observable(){

	var duration: String = ""
		set(value) {
			field = value
			setChangedAndNotify("duration")
		}

	var overview: String = ""
		set(value) {
			field = value
			setChangedAndNotify("overview")
		}

	var cover_url: String = ""
		set(value) {
			field = value
			setChangedAndNotify("cover_url")
		}

	var releaseYear: String = ""
		set(value) {
			field = value
			setChangedAndNotify("releaseYear")
		}

	var id: String = ""
		set(value) {
			field = value
			setChangedAndNotify("id")
		}

	var title: String = ""
		set(value) {
			field = value
			setChangedAndNotify("title")
		}

	var backdropsUrl: String = ""
		set(value) {
			field = value
			setChangedAndNotify("backdropsUrl")
		}


	private fun setChangedAndNotify(field: Any){
		setChanged()
		notifyObservers(field)
	}
}