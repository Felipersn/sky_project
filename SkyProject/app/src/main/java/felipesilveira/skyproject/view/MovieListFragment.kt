package felipesilveira.skyproject.view

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import felipesilveira.skyproject.adapter.MovieListAdapter
import felipesilveira.skyproject.databinding.FragmentMoviesListBinding
import felipesilveira.skyproject.viewModel.MoviesListViewModel

class MovieListFragment : Fragment() {

    lateinit var viewModel: MoviesListViewModel


    companion object {
        fun newInstance(viewModel: MoviesListViewModel): MovieListFragment {
            val fragment = MovieListFragment()
            fragment.viewModel = viewModel
            return fragment
        }
    }

    override fun onCreateView(@NonNull inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentMoviesListBinding = FragmentMoviesListBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.rvMoviesList.adapter = MovieListAdapter(emptyList())
        binding.rvMoviesList.layoutManager = GridLayoutManager(activity, 2)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        viewModel.load()
    }
}