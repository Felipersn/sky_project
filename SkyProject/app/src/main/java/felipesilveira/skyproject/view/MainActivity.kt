package felipesilveira.skyproject.view

import android.os.Bundle
import felipesilveira.skyproject.R
import felipesilveira.skyproject.connection.ApiInterface
import felipesilveira.skyproject.connection.DataSource
import felipesilveira.skyproject.connection.MoviesRepository
import felipesilveira.skyproject.utils.AppCompatActivityExt
import felipesilveira.skyproject.utils.NetworkUtils
import felipesilveira.skyproject.viewModel.MoviesListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivityExt() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addFragmentTo(R.id.content, createFragment())
        setSupportActionBar(toolbar)
    }

    fun createViewModel(): MoviesListViewModel {
        val dataSource = DataSource(NetworkUtils.getRetrofitConfiguration().create(ApiInterface::class.java))
        val repository = MoviesRepository(dataSource)
        return MoviesListViewModel(repository, applicationContext)
    }

    fun createFragment(): MovieListFragment {
        return MovieListFragment.newInstance(createViewModel())
    }
}
