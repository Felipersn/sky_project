package felipesilveira.skyproject.viewModel

import android.content.Context
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import felipesilveira.skyproject.R
import felipesilveira.skyproject.connection.MoviesRepository
import felipesilveira.skyproject.model.MoviesList

class MoviesListViewModel(val repository: MoviesRepository, val context: Context) {
    val movies = ObservableArrayList<MoviesList>()
    val loadingVisibility = ObservableBoolean(true)
    val message = ObservableField<String>()

    fun load() {
        loadingVisibility.set(true)
        message.set("")
        repository.listAll({ items ->
            movies.clear()
            movies.addAll(items)
            if (items.isEmpty()) {
                message.set(context.getString(R.string.emptyList))
            }
            loadingVisibility.set(false)
        }, {
            message.set(context.getString(R.string.failureLoadMovies))
            loadingVisibility.set(false)
        })
    }
}